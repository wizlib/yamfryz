
# Concept 

Query and update YAML files with a CLI

Treat a YAML file as a database with simple configurable query syntax.

Example, contact management

```yaml
John Smith:
  context: rotary
  notes: >-
    Wife is Martha. Kids are Alex (4) and Jamie (2). Lives in Connecticut.
```

```bash
yamf select 'John Smith'

yamf select 'Jo-'

yamf select --vals context=rotary
```

Queries return YAML so they can be piped

```bash
yamf select --vals context=rotary | yamf select 'Jo-'
```


